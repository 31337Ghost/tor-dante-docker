#!/bin/sh

set -e

: ${SOCKD_USER_NAME:=user}

echo $1 | grep -q ^sockd- || exec "$@"

case $1 in
    'sockd-username')
        if [ -z "${SOCKD_USER_PASSWORD}" ]; then
            echo "Set \$SOCKD_USER_PASSWORD variable please"
            exit 1
        fi

        if ! id -u $SOCKD_USER_NAME > /dev/null 2>&1; then
            adduser -D $SOCKD_USER_NAME
        fi
        echo $SOCKD_USER_NAME:$SOCKD_USER_PASSWORD |chpasswd

        exec sockd
    ;;
esac
