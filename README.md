# tor-dante-docker
Dante and tor in docker containers

# Run docker-compose
```
git clone https://gitlab.com/31337Ghost/tor-dante-docker.git
cd tor-dante-docker
docker-compose pull
docker-compose up -d
```
Notice that in some cases tor client needs some time to bootstrap

# Port and user:password
0.0.0.0:3127 and testuser:testpassword (editable in docker-compose.yml)

# Logs
```
docker-compose logs -f
 ```
